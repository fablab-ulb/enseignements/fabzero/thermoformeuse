# Thermoformeuse

Machine : Mayku Formbox

[Chaine YouTube du fabricant Mayku](https://www.youtube.com/watch?v=MTZ5FunrcDY&list=PLXD9bFVvI9rqEF9vut1g1tS9BtTPMAbdM&pp=iAQB)

Il existe deux type de feuilles disponible à l'accueil : 

![Sheet](img/THERMO_sheet.png)

- Form sheet [HIPS]
    - Blanche
    - Prête à être peinte
    - Durable
    - Reproduisent les détails fins

- Clear sheet [PETG]
    - Transparente
    - Sans danger pour les aliments

1. Brancher la termoformeuse.

2. Connecter l'aspirateur directement dans la prise à l'arrière de la thermoformeuse et le mettre sur ON.

*L'aspirateur Festool à une connectique britanique pour le branchement*

*C'est normal qu'il ne s'allume pas, la thermoformeuse s'occupe de l'activer quand ça sera necessaire*

3. Raccorder le tube de l'aspirateur à la thermoformeuse

4. Régler le timer et la temperature de chauffe

*Ces deux paramètres varient en fonction du type de feuille*

*La LED clignote le temps du préchauffage, une fois allumée en continu et verte, la machine est prête*

![Timer and temperature](img/THERMO_timer_and_temperature.png)

5. Si necessaire, retirer le plastic protecteur de la Clear Sheet

6. Utiliser les poignées extérieures pour relever la plaque supérieure jusqu'à entendre un "CLAC" pour la blquer en haut

![Up](img/THERMO_up.png)
![Clac](img/THERMO_Clac.png)

7. Placer la Sheet sur la plaque inférieure

![Sheet place](img/THERMO_sheet_place.png)

8. Redescendre la plaque supérieur pour coincer la Sheet entre les deux plaques, bloquer les poignés pour assembler les deux plaques.

9. Remonter les deux plaques en maintenant les deux poignées jusqu'en haut.

10. Appuyer sur le "Start timer"

![Start](img/THERMO_start.png)

11. Placer l'objet sur la grille

![Object](img/THERMO_object.png)

12. Attendre que la Sheet chauffe, une fois que le timer est fini **OU/ET** que la sheet courbe sur au moins 1cm en dessous du niveau de la plaque.

13. Abaisser les deux plaques jusqu'en bas en une fois.

![Thermo](img/THERMO_thermo.png)

14. Attendre 10 secondes avant de remonter la plaque supérieur

![Up final](img/THERMO_up_final.png)

15. Retirer l'objet

![End](img/THERMO_end.png)
